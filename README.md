A quick demo of a few features in Unity.
To install the Android SDK:
--Download the SDK from the Android website and install (REMEMBER WHERE YOU INSTALLED IT TO)
--In Unity go to File>Build Settings and select Android
--Select "Switch Platform"
--Download the UnitySetup-Android executable and run
--Reopen Unity and go to Edit>Project Settings>Player then under Identification set the Bundle Identifier to "com.YOUR_COMPANY_NAME.YOUR_APP_NAME"
--Open Edit>Preferences>External Tools and select Browse next to Android SDK to select the Android SDK tools
--Download and install the Java SDK (JDK)
--Build and Run