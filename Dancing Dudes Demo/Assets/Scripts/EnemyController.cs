﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class EnemyController : MonoBehaviour {

    private GameController m_GameController;
    public int m_nEnemyScore; // points awared on death

	// Use this for initialization
	void Start () {
        m_GameController = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
	}

    void OnMouseDown()
    {
        //GetComponent<TapGesture>().Tapped -= tapHandler;  // be sure to unregister the tap handler
        m_GameController.OnEnemyKilled(m_nEnemyScore);
        Destroy(gameObject);
    }


}
