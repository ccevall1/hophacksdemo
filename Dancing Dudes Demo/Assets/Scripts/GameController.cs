﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    private int m_nScore;
    public int m_nStartingEnemyCount;
    private int m_nCurrEnemyCount;
    public GameObject m_EnemyPrefab;
    private float m_fPiSlices;
    public int m_nRadius;

    private Text m_ScoreText;

	// Use this for initialization
	void Start () {
        m_nCurrEnemyCount = m_nStartingEnemyCount;
        m_ScoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        m_fPiSlices = 2*Mathf.PI / m_nStartingEnemyCount;
	    for (int i = 0; i < m_nStartingEnemyCount; i++)
        {
            Instantiate(m_EnemyPrefab, new Vector3(m_nRadius * Mathf.Cos(i * m_fPiSlices), 
                                                    3, m_nRadius * Mathf.Sin(i * m_fPiSlices)), 
                                       Quaternion.identity);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnEnemyKilled(int nEnemyScore)
    {
        m_nCurrEnemyCount -= 1;
        m_nScore += nEnemyScore;
        m_ScoreText.text = "Score: " + m_nScore;
        if (m_nCurrEnemyCount <= 0)
        {
            // TODO- handle end game
            PlayerPrefs.SetInt("Score", m_nScore);
            SceneManager.LoadScene(1);
        }
    }

}
