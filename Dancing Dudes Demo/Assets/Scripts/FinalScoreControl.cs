﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FinalScoreControl : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        int score = PlayerPrefs.GetInt("Score", 0);
        GetComponent<Text>().text = "Final Score: " + score;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
