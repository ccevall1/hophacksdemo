﻿using UnityEngine;
using System.Collections;

public class LightController : MonoBehaviour {

    public int m_nTimeChange;
    private Light m_Light;

	// Use this for initialization
	void Start () {
        m_Light = GetComponent<Light>();
        StartCoroutine(ChangeColor());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator ChangeColor()
    {
        while (true)
        {
            yield return new WaitForSeconds(m_nTimeChange);
            m_Light.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        }
    }
}
