﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

    private Transform m_tCameraPos;
    public float m_fSpeed;

	// Use this for initialization
	void Start () {
        m_tCameraPos = GetComponent<Camera>().transform;
	}
	
	// Update is called once per frame
	void Update () {
        //Let's rotate the camera
        m_tCameraPos.Rotate(Vector3.up, Time.deltaTime * m_fSpeed);
	}
}
